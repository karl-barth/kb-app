window.addEventListener('DOMContentLoaded', () => {
    pbEvents.subscribe('pb-popover-show', null, (ev) => {
        const popover = ev.detail.source;
        const corresp = popover.dataset.corresp;
        if (corresp) {
            const parts = corresp.split('-');
            const id = parts[2];
            const api = parts[1];
            const url = `https://meta.karl-barth.ch/api/${api}/${id}`;
            fetch(url)
            .then((response) => {
                if (!response.ok) {
                    throw new Error('Network response was not OK');
                }
                return response.json();
            })
            .then((json) => {
                popover.alternate = json.data.asHtml.replaceAll(/<\/?a\s*[^>]*>|<\/?span\s*[^>]*>/g, '');
            })
            .catch((error) => {
                console.error(`Error retrieving bibl ${id}`);
            });
        }
    });

    pbEvents.subscribe('pb-update', 'aside', (ev) => {
        const volume = ev.detail.root.querySelector('.volume');
        document.querySelector('.breadcrumbs .volume').innerHTML = volume.innerHTML;

        const title = ev.detail.root.querySelector('.contentTitle');
        document.querySelector('.breadcrumbs .title').innerHTML = title.innerHTML;

        const navNext = ev.detail.root.querySelector('.navigation .next');
        document.querySelector('.doc-forward').innerHTML = navNext.innerHTML;
        navNext.parentNode.removeChild(navNext);
        
        const navPrev = ev.detail.root.querySelector('.navigation .previous');
        document.querySelector('.doc-backward').innerHTML = navPrev.innerHTML;
        navPrev.parentNode.removeChild(navPrev);
    });
});
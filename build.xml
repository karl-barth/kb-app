<?xml version="1.0" encoding="UTF-8"?>
<project name="kb" default="xar">    
    <property name="npm" value="npm" />
    <property name="build.dir" value="build"/>    
    <property name="git.repo.path" value="${basedir}/.git"/>
    <available file="${git.repo.path}" type="dir" property="git.present" />
    <property name="scripts.dir" value="node_modules/@teipublisher/pb-components/dist" />
    

    <target name="clean">
        <delete includeemptydirs="true" failonerror="false">
          <fileset dir="${build.dir}" includes="**/*"/>
        </delete>
    </target>
  


    <target name="prepare-components">
        <mkdir dir="${app.build.dir}"/>
        <copy todir="${app.build.dir}">
            <fileset dir="${basedir}"
                excludes="${build.dir}/**,build.xml,.*,repo.xml.tmpl,node_modules/**,package*.json,local.node-exist.json,gulpfile.js,.devcontainer/**,.idea/**"/>
        </copy>  
        <copy todir="${app.build.dir}/resources/scripts">
            <fileset dir="${scripts.dir}">
                <include name="*.js" />
                <include name="*.map" />
            </fileset>
        </copy>
        <copy file="node_modules/leaflet/dist/leaflet-src.js" todir="${app.build.dir}/resources/lib"/>
        <copy file="node_modules/leaflet/dist/leaflet.css" todir="${app.build.dir}/resources/css/leaflet" />
        <copy file="node_modules/leaflet/dist/leaflet.css" todir="${app.build.dir}/resources/css/vendor" />        
        <copy todir="${app.build.dir}/resources/images/leaflet">
            <fileset dir="node_modules/leaflet/dist/images" />
        </copy>
        <copy file="node_modules/leaflet.markercluster/dist/leaflet.markercluster-src.js" todir="${app.build.dir}/resources/lib"/>
        <copy todir="${app.build.dir}/resources/css/leaflet">
            <fileset dir="node_modules/leaflet.markercluster/dist">
                <include name="*.css"/>
            </fileset>
        </copy>
        <copy todir="${app.build.dir}/resources/images/openseadragon">
            <fileset dir="node_modules/openseadragon/build/openseadragon/images" />
        </copy>
        <copy file="node_modules/openseadragon/build/openseadragon/openseadragon.min.js"
            todir="${app.build.dir}/resources/lib" />
        <copy todir="${app.build.dir}/resources/css/prismjs">
            <fileset dir="node_modules/prismjs/themes" />
        </copy>
        <copy todir="${app.build.dir}/resources/css/tom-select">
            <fileset dir="node_modules/tom-select/dist/css">
                <include name="tom-select.*.min.css"/>
            </fileset>
        </copy>
        <copy todir="${app.build.dir}/resources/i18n/common">
            <fileset dir="node_modules/@teipublisher/pb-components/i18n/common" />
        </copy>
    </target>

    <target name="prepare">
        <property name="project.version" value="0.2" />
        <property name="project.app" value="kb" />
        <property name="project.description" value="Karl Barth Pilot Edition" />
        <property name="app.build.dir" value="${build.dir}/${project.app}-${project.version}"/>
        <antcall target="prepare-components"/>
        <copy todir="${app.build.dir}" overwrite="true" verbose="true">
            <fileset file="*.xml.tmpl" />
            <filterchain>
                <replacetokens>
                    <token key="version" value="${project.version}" />
                    <token key="description" value="${project.description}" />
                    <token key="abbrev" value="${project.app}" />
                    <token key="commit-id" value="${git.revision}" />
                    <token key="commit-time" value="${git.time}" />
                </replacetokens>
                <tokenfilter>
                    <!-- until we move template processing to XSLT, take care with special
                    characters -->
                    <replacestring from="&amp;" to="&amp;amp;" />
                </tokenfilter>
            </filterchain>
            <globmapper from="*.tmpl" to="*" />
        </copy>
        

    </target>

    <target name="production-prepare">
        <property name="project.version" value="0.2" />
        <property name="project.app" value="kbga" />
        <property name="project.description" value="Karl Barth Edition Production" />
        <property name="app.build.dir" value="${build.dir}/${project.app}-${project.version}"/>
        <antcall target="prepare-components"/>
        <replaceregexp file="${app.build.dir}/modules/config.xqm" match="\$config:data-root\s*:=.*$"
            replace='$config:data-root := "/db/apps/kb-data";' flags="m" />        
        <replace file="${app.build.dir}/resources/odd/barth.odd" token="/db/apps/kb-latest-version"
            value="/db/apps/kb-data" />        
        <replace file="${app.build.dir}/resources/odd/configuration.xml"
            token="xmldb:exist:///db/apps/kb/modules/ext-html.xql"
            value="xmldb:exist:///db/apps/kbga/modules/ext-html.xql" />
        <copy todir="${app.build.dir}" overwrite="true" verbose="true">
            <fileset file="*.xml.tmpl" />
            <filterchain>
                <replacetokens>
                    <token key="version" value="${project.version}" />
                    <token key="description" value="${project.description}" />
                    <token key="abbrev" value="${project.app}" />
                    <token key="commit-id" value="${git.revision}" />
                    <token key="commit-time" value="${git.time}" />
                </replacetokens>
                <tokenfilter>
                    <!-- until we move template processing to XSLT, take care with special
                    characters -->
                    <replacestring from="&amp;" to="&amp;amp;" />
                </tokenfilter>
            </filterchain>
            <globmapper from="*.tmpl" to="*" />
        </copy>

    </target>

    <target name="xar-production" depends="clean,npm.install,git.revision,production-prepare,build-xar" />

    <target name="xar" depends="clean,npm.install,git.revision,prepare,build-xar" />

    <target name="build-xar">
        <zip basedir="${app.build.dir}" destfile="${build.dir}/${project.app}-${project.version}.xar"/>
    </target>

    <target name="npm.install">
        <exec executable="${npm}" outputproperty="npm.output">
            <arg line="install" />
        </exec>
        <echo message="${npm.output}" />
    </target>

  

    <target name="git.revision" description="Store git revision in ${repository.version}"
        if="git.present">
        <exec executable="git" outputproperty="git.revision" failifexecutionfails="false"
            errorproperty="">
            <arg value="--git-dir=${git.repo.path}" />
            <arg value="rev-parse" />
            <arg value="HEAD" />
        </exec>
        <condition property="repository.version" value="${git.revision}" else="unknown">
            <and>
                <isset property="git.revision" />
                <length string="${git.revision}" trim="yes" length="0" when="greater" />
            </and>
        </condition>
        <echo>Git repo: ${repository.version}</echo>
        <exec executable="git" outputproperty="git.time" failifexecutionfails="false"
            errorproperty="">
            <arg value="--git-dir=${git.repo.path}" />
            <arg value="show" />
            <arg value="-s" />
            <arg value="--format=%ct" />
            <arg value="${git.revision}" />
        </exec>
        <echo>Git time: ${git.time}</echo>
    </target>    
</project>

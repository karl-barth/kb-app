xquery version "3.1";

module namespace api="http://teipublisher.com/api/custom";

declare namespace tei="http://www.tei-c.org/ns/1.0";

import module namespace bible="http://teipublisher.com/api/bible" at "bible.xql";
import module namespace templates="http://exist-db.org/xquery/html-templating";
import module namespace router="http://exist-db.org/xquery/router";
import module namespace rutil="http://exist-db.org/xquery/router/util";
import module namespace config="http://www.tei-c.org/tei-simple/config" at "config.xqm";
import module namespace pages="http://www.tei-c.org/tei-simple/pages" at "lib/pages.xql";
import module namespace app="teipublisher.com/app" at "app.xql";
import module namespace pm-config="http://www.tei-c.org/tei-simple/pm-config" at "pm-config.xql";
import module namespace vapi="http://teipublisher.com/api/view" at "lib/api/view.xql";
import module namespace errors = "http://exist-db.org/xquery/router/errors";
import module namespace facets="http://teipublisher.com/facets" at "facets.xql";
import module namespace tpu="http://www.tei-c.org/tei-publisher/util" at "lib/util.xql";
import module namespace ext-html="http://karlbarth.ch/apps/kb/ext-html.xql" at "ext-html.xql";
import module namespace tei-nav="http://www.tei-c.org/tei-simple/navigation/tei" at "navigation-tei.xql";


(:~
 : Keep this. This function does the actual lookup in the imported modules.
 :)
declare function api:lookup($name as xs:string, $arity as xs:integer) {
    try {
        function-lookup(xs:QName($name), $arity)
    } catch * {
        ()
    }
};

declare function api:resolve($request as map(*)) {
    let $byId := collection($config:data-root)/id("kbga-texts-" || $request?parameters?id)
    let $root :=
        if ($byId) then
            $byId
        else
            doc($config:data-root || "/" || $request?parameters?collection || "/" || $request?parameters?idOrDoc)
    return
        if ($root) then
            vapi:view(
                map:merge((
                    $request, 
                    map { 
                        "parameters": map:merge((
                            $request?parameters,
                            map {
                                "docid": substring-after(document-uri(root($root)), $config:data-root || '/'),
                                "id": if ($byId) then $request?parameters?idOrDoc else (),
                                "title": $byId//tei:teiHeader//tei:title[@type='citation_line_1']/string()
                            }
                        ))
                    }
                ))
            )
        else
            error($errors:NOT_FOUND, "Document " || $request?parameters?id || " not found")
};

(:
    55003.xml/p020 - andere Datei, selbes Volume
    ./p020 oder p020 - selbes Volume
    ../vol-01/p020 - Seite in anderem Volume
    ../vol-01/p020#fn_01 - Fußnote auf Seite in anderem Volume
:)
declare function api:volume-ref($request as map(*)) {
    let $volume := $request?parameters?volume
    let $page := $request?parameters?page
    let $doc := head(collection($config:data-root || "/vol-" || $volume)/id($page))
    return
        if ($doc) then
            let $id := root($doc)/tei:TEI/@xml:id
            return
                response:redirect-to(xs:anyURI("../../texts/" || substring-after($id, 'kbga-texts-') || "?id=" || $page))
        else
            error($errors:NOT_FOUND, "Page " || $page || " in volume " || $volume || " not found")
};

declare function api:volume-redirect($request as map(*)) {
    let $volume := $request?parameters?volume
    return
       response:redirect-to(xs:anyURI("../texts/?sort=file&amp;facet-volume=" ||$volume))
};


declare function api:image($request as map(*)) {
    let $log := util:log('INFO', 'Loading image: ' || $request?parameters?image)
    let $imagePath := $config:data-root || "/vol-" || $request?parameters?volume || "/" ||
        $request?parameters?image
    return
        response:stream-binary(util:binary-doc($imagePath), "image/jpeg", $request?parameters?image)
};

declare function api:table-of-contents($request as map(*)) {
    <ul>
    {
        let $doc := xmldb:decode-uri($request?parameters?id)
        let $view := head(($request?parameters?view, $config:default-view))
        let $xml := pages:load-xml($view, (), $doc)        
        let $volume := (root($xml?data)//tei:titleStmt/tei:title[@type="volume"][@n="vol-40"] | 
        root($xml?data)//tei:titleStmt/tei:title[@type="volume"][@n="vol-47"])/@n
        for $doc in collection($config:data-default)//tei:text[ft:query(., "volume:" || $volume, map { 
            "leading-wildcard": "yes",
            "filter-rewrite": "yes",
            "fields": ("position", "title")
        })]
        order by ft:field($doc, "position", "xs:integer")
        let $nested := $doc//tei:body/tei:div
        return
            api:output-nested($nested)
        (: if ($nested) then
             <li> 
                <pb-link path="{$volume}/{util:document-name($doc)}" emit="transcription">{ft:field($doc, "title")}
                <span>S. {$page}</span></pb-link>
                <ul>{api:output-nested($nested)}</ul>
            </li>
        else
            
            <li>
                <pb-link path="{$volume}/{util:document-name($doc)}" emit="transcription">{ft:field($doc, "title")}
                <span>S. {$page}</span></pb-link>
            </li> :)
    }
    </ul>
};

declare function api:output-nested($divs as element(tei:div)*) {
    for $div in $divs
    return
        if ($div/tei:head) then
            let $volume := root($div)//tei:titleStmt/tei:title[@type="volume"]/@n
            let $pageId := ($div//tei:pb)[1]/@xml:id/string()
            let $page :=
                if (starts-with($pageId, 'pr')) then 
                    ext-html:latDezi(number(replace($pageId, "^pr?(\d+)$", "$1")))
                else
                    number(replace($pageId, "^p(\d+)$", "$1"))
            return
                <li>
                    <pb-link path="{$volume}/{util:document-name($div)}" node-id="{util:node-id($div)}" emit="transcription">{$div/tei:head/string()} <span>S. {$page}</span></pb-link>
                    {
                        if ($div/tei:div) then
                            <ul>{api:output-nested($div/tei:div)}</ul>
                        else
                            ()
                    }
                </li>
        else
            api:output-nested($div/tei:div)
};

declare function api:person-info($request as map(*)) {
    for $ref in tokenize(xmldb:decode-uri($request?parameters?id), '\s+')
    let $person := id($ref, doc($config:data-root || '/kbga-people.xml'))
    return
        if (exists($person)) then
            <div class="entity-details">
                {$pm-config:web-transform($person, map { "root": $person }, $config:default-odd)}
                <a href="../actors/{substring-after($person/@xml:id, 'kbga-actors-')}" class="link">Details</a>
            </div>
        else
            <p>Keine weiteren Informationen.</p>
};

declare function api:organization-info($request as map(*)) {
    for $ref in tokenize(xmldb:decode-uri($request?parameters?id), '\s+')
    let $org := id($ref, doc($config:data-root || '/kbga-organisations.xml'))
    return
        if (exists($org)) then
            <div class="entity-details">
                {$pm-config:web-transform($org, map { "root": $org }, $config:default-odd)}
                <a href="../actors/{substring-after($org/@xml:id, 'kbga-actors-')}" class="link">Details</a>
            </div>
        else
            <p>Keine weiteren Informationen.</p>
};

declare function api:place-info($request as map(*)) {
    for $ref in tokenize(xmldb:decode-uri($request?parameters?id), '\s+') 
    let $place := id($ref, doc($config:data-root || '/kbga-places.xml'))
    return
        if (exists($place)) then
            <div class="entity-details">
                {$pm-config:web-transform($place, map { "root": $place }, $config:default-odd)}
                <a href="../places/{substring-after($place/@xml:id, 'kbga-places-')}" class="link">Details</a>
            </div>
        else
            <p>Keine weiteren Informationen.</p>
};

declare function api:song-info($request as map(*)) {
    for $ref in tokenize(xmldb:decode-uri($request?parameters?id), '\s+') 
    let $song := id($ref, doc($config:data-root || '/kbga-songs.xml'))
    return
        if (exists($song)) then
            <div class="entity-details">
                {$pm-config:web-transform($song, map { "root": $song }, $config:default-odd)}
                <a href="../songs/{substring-after($song/@xml:id, 'kbga-songs-')}" class="link">Details</a>
            </div>
        else
            <p>Keine weiteren Informationen.</p>
};

declare function api:bibl-info($request as map(*)) {
    for $ref in tokenize(xmldb:decode-uri($request?parameters?id), '\s+') 
    let $bibl := id($ref, doc($config:data-root || '/kbga-bibls.xml'))
    return
        if (exists($bibl)) then
            <div class="entity-details">
                {$pm-config:web-transform($bibl, map { "root": $bibl, "mode": "popover" }, 'barth-register.odd')}
                <a href="../biblio/{substring-after($bibl/@xml:id, 'kbga-bibls-')}" class="link">Details</a>
            </div>
        else
            <p>Keine weiteren Informationen.</p>
};

declare function api:abbr-info($request as map(*)) {
    for $ref in tokenize(xmldb:decode-uri($request?parameters?id), '\s+') 
    let $place := id($ref, doc($config:data-root || '/kbga-abbreviations.xml'))
    return
        if (exists($place)) then
            $pm-config:web-transform($place, map { "root": $place }, $config:default-odd)
        else
            <p>Keine weiteren Informationen.</p>
};

declare function api:actors($request as map(*)) {
    let $search := normalize-space($request?parameters?search)
    let $letterParam := $request?parameters?category
    let $view := $request?parameters?view
    let $sortDir := $request?parameters?dir
    let $limit := $request?parameters?limit
    let $people :=
        if ($search and $search != '') then
            if ($view = 'organisations') then
                doc($config:data-root || "/kbga-organisations.xml")//tei:listOrg/tei:org[ft:query(., 'name:(' || $search || '*)')]
            else
                doc($config:data-root || "/kbga-people.xml")//tei:listPerson/tei:person[ft:query(., 'name:(' || $search || '*)')]
        else
            if ($view = 'organisations') then
                doc($config:data-root || "/kbga-organisations.xml")//tei:listOrg/tei:org
            else
                doc($config:data-root || "/kbga-people.xml")//tei:listPerson/tei:person
    let $byKey := for-each($people, function($person as element()) {
        let $name := 
            if ($view = 'organisations') then
                $person/tei:orgName[@type="full"]
            else
                $person/tei:persName[@type="full"]
        let $label := $name/text()
        let $sortKey :=
            if (starts-with($label, "von ")) then
                substring($label, 5)
            else
                $label
        return
            [lower-case($sortKey), $label, $person]
    })
    let $sorted := api:sort($byKey, $sortDir)
    let $letter := 
        if (count($people) < $limit) then 
            "Alle"
        else if ($letterParam = '') then
            substring($sorted[1]?1, 1, 1) => upper-case()
        else
            $letterParam
    let $byLetter :=
        if ($letter = 'Alle') then
            $sorted
        else
            filter($sorted, function($entry) {
                starts-with($entry?1, lower-case($letter))
            })
    return
        map {
            "items": api:output-person($byLetter, $letter, $view, $search),
            "categories":
                if (count($people) < $limit) then
                    []
                else array {
                    for $index in 1 to string-length('ABCDEFGHIJKLMNOPQRSTUVWXYZ')
                    let $alpha := substring('ABCDEFGHIJKLMNOPQRSTUVWXYZ', $index, 1)
                    let $hits := count(filter($sorted, function($entry) { starts-with($entry?1, lower-case($alpha))}))
                    where $hits > 0
                    return
                        map {
                            "category": $alpha,
                            "count": $hits
                        },
                    map {
                        "category": "Alle",
                        "count": count($sorted)
                    }
                }
        }
};

declare function api:output-person($list, $letter as xs:string, $view as xs:string, $search as xs:string?) {
    array {
        for $person in $list
        let $letterParam := if ($letter = "Alle") then substring($person?2, 1, 1) else $letter
        let $params := "category=" || $letterParam || "&amp;view=" || $view || "&amp;search=" || $search
        return
            <span class="register-item">
                <a href="{substring-after($person?3/@xml:id, 'kbga-actors-')}?{$params}">{$person?2}</a>
            </span>
    }
};

declare function api:songs($request as map(*)) {
    let $search := normalize-space($request?parameters?search)
    let $letterParam := $request?parameters?category
    let $limit := $request?parameters?limit
    let $ft-opts := map {
        "leading-wildcard": "yes",
        "filter-rewrite": "yes",
        "fields": "lname"
    }
    let $songs :=
        if ($search and $search != '') then
            doc($config:data-root || "/kbga-songs.xml")//tei:bibl[ft:query(., 'name:(' || $search || '*)', $ft-opts)]
        else
            doc($config:data-root || "/kbga-songs.xml")//tei:bibl[ft:query(., 'name:*', $ft-opts)]
    let $sorted := sort($songs, "?lang=de-DE", function($song) { lower-case($song/tei:title) })
    let $letter := 
        if (count($songs) < $limit) then 
            "Alle"
        else if ($letterParam = '') then
            substring($sorted[1]/tei:title, 1, 1) => upper-case()
        else
            $letterParam
    let $byLetter :=
        if ($letter = 'Alle') then
            $sorted
        else
            filter($sorted, function($entry) {
                starts-with(ft:field($entry, 'name'), $letter)
            })
    return
        map {
            "items": api:output-song($byLetter, $letter, $search),
            "categories":
                if (count($songs) < $limit) then
                    []
                else array {
                    for $index in 1 to string-length('ABCDEFGHIJKLMNOPQRSTUVWXYZ')
                    let $alpha := substring('ABCDEFGHIJKLMNOPQRSTUVWXYZ', $index, 1)
                    let $hits := 
                        filter($sorted, function($entry) {
                            starts-with(ft:field($entry, 'name'), $alpha)
                        }) => count()
                    where $hits > 0
                    return
                        map {
                            "category": $alpha,
                            "count": $hits
                        },
                    map {
                        "category": "Alle",
                        "count": count($sorted)
                    }
                }
        }
};

declare function api:output-song($list, $letter as xs:string, $search as xs:string?) {
    array {
        for $song in $list
        let $title := $song/tei:title[1]/string()
        let $letterParam := if ($letter = "Alle") then substring($title, 1, 1) else $letter
        let $params := "category=" || $letterParam  || "&amp;search=" || $search
        return
            <span class="register-item">
                <a href="{substring-after($song/@xml:id, 'kbga-songs-')}?{$params}">{$title}</a>
            </span>
    }
};

declare function api:get-sort-key($node as element()) {
    let $authors := if ($node/tei:author) then $node/tei:author else $node/tei:editor
    let $title := $node/tei:title[1]
    let $names := for $author in $authors return if ($author/@ref) then 
        doc($config:data-root || '/kbga-people.xml')/id($author/@ref)/tei:persName[not(@type)]/string() 
        else tokenize(replace($author, '[-\[]', ''), '\s+')[last()]
    return
        if ($authors) then lower-case(string-join($names, ' ')) else  '~' || $title
    };

declare function api:bibls($request as map(*)) {
    let $search := normalize-space($request?parameters?search)
    let $letterParam := $request?parameters?category
    let $limit := $request?parameters?limit
    let $bibls :=
        if ($search and $search != '') then
            doc($config:data-root || "/kbga-bibls.xml")//tei:bibl[ft:query(., 'status:autorisiert AND citation:(' || $search || '*)')]
        else
            doc($config:data-root || "/kbga-bibls.xml")//tei:bibl[ft:query(., 'status:autorisiert')]
    let $sorted := sort($bibls, "?lang=de-DE", function($bibl) { api:get-sort-key($bibl) })
    let $letter := 
        if (count($bibls) < $limit) then 
            "Alle"
        else if ($letterParam = '') then
            substring(api:get-sort-key($sorted[1]), 1, 1) => upper-case()
        else
            $letterParam
    let $byLetter :=
        if ($letter = 'Alle') then
            $sorted 
        else
          if ($letter = ('~', 'Anon')) then filter($sorted, function($entry) { 
                if (starts-with(api:get-sort-key($entry), '~')) then $entry else ()
                })
            else filter($sorted, function($entry) {starts-with(ft:field($entry, 'firstAuthor'), lower-case($letter))}) 
    return
        map {
            "items": api:output-bibl($byLetter, $letter, $search),
            "categories":
                if (count($bibls) < $limit) then
                    []
                else array {
                    for $index in 1 to string-length('ABCDEFGHIJKLMNOPQRSTUVWXYZ~')
                    let $alpha := substring('ABCDEFGHIJKLMNOPQRSTUVWXYZ~', $index, 1)
                    let $hits := if ($alpha eq '~') then filter($sorted, function($entry) {
                            starts-with(api:get-sort-key($entry), '~')
                        }) => count() else 
                        filter($sorted, function($entry) {
                           starts-with(ft:field($entry, 'firstAuthor'), lower-case($alpha)) 
                        }) => count()
                    let $category := if ($alpha eq '~') then 'Anon' else $alpha
                    where $hits > 0
                    return
                        map {
                            "category": $category,
                            "count": $hits
                        },
                    map {
                        "category": "Alle",
                        "count": count($sorted)
                    }
                }
        }
};

declare function api:output-bibl($list, $letter as xs:string, $search as xs:string?) {
    array {
        for $bibl in $list
        let $title := ext-html:output-reference($bibl, 60, 'inactive')
        let $letterParam := if ($letter = "Alle") then substring(api:get-sort-key($bibl), 1, 1) else $letter
        let $params := "category=" || $letterParam  || "&amp;search=" || $search
        return
            <span class="register-item">
                <a href="{substring-after($bibl/@xml:id, 'kbga-bibls-')}?{$params}">{$title}</a>
            </span>
    }
};

declare function api:bible($request as map(*)){
    let $refs :=  collection($config:data-root)//tei:ref[@subtype eq 'bible']
    (: let $mentionedBooks := for $x in $refs return ft:field($x, 'book'):)
    let $mentionedBooks := distinct-values(for $target in $refs/@target return ext-html:get-book($target))
    let $maps := $bible:BIBLIA?*[(?barth = $mentionedBooks) or (?abbreviations = $mentionedBooks)]
    let $sorted:= sort($maps, "?lang=de-DE", function($x) {$x?position})
    return
        for $map in $sorted
        let $type := if ($map?type eq 'AT') then 'Altes Testament' else if ($map?type eq 'Apo') then 'Apokryphen' else 'Neues Testament'
        group by $type  
        order by $type
        let $titles := $map?name
        return
           ( <h2>{$type}</h2>,
           <div class="register-list">{
            for $title at $pos in $titles  
            let $id := $map?barth[$pos]
            return
            <a href="{$id}" class="register-item">{$title}</a> }</div>
           )

};

declare function api:places($request as map(*)) {
    let $search := normalize-space($request?parameters?search)
    let $letterParam := $request?parameters?category
    let $limit := $request?parameters?limit
    let $ft-opts := map {
        "leading-wildcard": "yes",
        "filter-rewrite": "yes",
        "fields": "lname"
    }
    let $places :=
        if ($search and $search != '') then
            doc($config:data-root || "/kbga-places.xml")//tei:place[ft:query(., 'name:(' || $search || '*)', $ft-opts)]
                [tei:location/tei:geo]
        else
            doc($config:data-root || "/kbga-places.xml")//tei:place[ft:query(., 'name:*', $ft-opts)]
                [tei:location/tei:geo]
    let $sorted := sort($places, "?lang=de-DE", function($place) { lower-case($place/tei:placeName[not(@type)]) })
    let $letter := 
        if (count($places) < $limit) then 
            "Alle"
        else if ($letterParam = '') then
            substring($sorted[1]/tei:placeName[not(@type)], 1, 1) => upper-case()
        else
            $letterParam
    let $byLetter :=
        if ($letter = 'Alle') then
            $sorted
        else
            filter($sorted, function($entry) {
                starts-with(ft:field($entry, 'lname'), lower-case($letter))
            })
    return
        map {
            "items": api:output-place($byLetter, $letter, $search),
            "categories":
                if (count($places) < $limit) then
                    []
                else array {
                    for $index in 1 to string-length('ABCDEFGHIJKLMNOPQRSTUVWXYZ')
                    let $alpha := substring('ABCDEFGHIJKLMNOPQRSTUVWXYZ', $index, 1)
                    let $hits := 
                        filter($sorted, function($entry) {
                            starts-with(ft:field($entry, 'lname'), lower-case($alpha))
                        }) => count()
                    where $hits > 0
                    return
                        map {
                            "category": $alpha,
                            "count": $hits
                        },
                    map {
                        "category": "Alle",
                        "count": count($sorted)
                    }
                }
        }
};

declare function api:places-all($request as map(*)) {
    let $places := doc($config:data-root || "/kbga-places.xml")//tei:listPlace/tei:place
    return 
        array { 
            for $place in $places
            where $place/tei:location/tei:geo/text()
                let $tokenized := tokenize($place/tei:location/tei:geo)
                return
                    map {
                        "latitude":$tokenized[1],
                        "longitude":$tokenized[2],
                        "label":$place/tei:placeName[@type='full']/string()
                    }
            }
};

declare function api:output-place($list, $category as xs:string, $search as xs:string?) {
    array {
        for $place in $list
        let $label := $place/tei:placeName[@type='full']/string()
        let $categoryParam := if ($category = "all") then substring($label, 1, 1) else $category
        let $params := "search=" || $search
        let $coords := tokenize($place/tei:location/tei:geo)
        return
            <span class="place register-item">
                <a href="{substring-after($place/@xml:id, 'kbga-places-')}?{$params}">{$label}</a>
                <pb-geolocation latitude="{$coords[1]}" longitude="{$coords[2]}" label="{$label}" emit="map" event="click">
                    { if ($place/@type != 'approximate') then attribute zoom { 9 } else () }
                    <iron-icon icon="maps:map"></iron-icon>
                </pb-geolocation>
            </span>
    }
};

declare function api:sort($people as array(*)*, $dir as xs:string) {
    let $sorted :=
        sort($people, "?lang=de-DE", function($entry) {
            $entry?1
        })
    return
        if ($dir = "asc") then
            $sorted
        else
            reverse($sorted)
};

declare function api:facets-volumes($request as map(*)) {
    let $hits := session:get-attribute($config:session-prefix || ".hits")
    where count($hits) > 0
    return
        <div>
        {
            for $config in $config:facets-volumes?*
            return
                facets:display($config, $hits)
        }
        </div>
};

declare function api:facets($request as map(*)) {
    if (exists($request?parameters?query) or exists($request?parameters?value)) then
        let $hits := session:get-attribute($config:session-prefix || ".hits")
        where count($hits) > 0
        let $dimension :=
            switch($request?parameters?type)
                case "people" return "person"
                case "terms" return "term"
                default return "place"
        let $facets := ft:facets($hits, $dimension, ())
        let $people :=        
            for $key in
                if (exists($request?parameters?value)) then $request?parameters?value else map:keys($facets)
            let $person := id($key, doc($config:data-root || "/kbga-" || $request?parameters?type || ".xml"))
            let $name := 
                switch($request?parameters?type)
                    case "people" return $person/tei:persName[@type='full']
                    case "terms" return $person/tei:catDesc
                    default return $person/tei:placeName[@type='full']
            return
                map {
                    "text": $name/string(),
                    "freq": $facets($key),
                    "value": $key
                }
        let $filtered := filter($people, function($person) {
            matches($person?text, '(?:^|\W)' || $request?parameters?query, 'i')
        })
        return
            array { $filtered }
    else
        []
};

declare function api:volumes-facet($request as map(*)) {
    if (exists($request?parameters?query)) then
        array {
            let $hits := session:get-attribute($config:session-prefix || ".hits")
            where count($hits) > 0
            let $facets := ft:facets($hits, "volume", ())
            for $key in map:keys($facets)
            where matches($key, '(?:^|\W)' || $request?parameters?query, 'i')
            return
                map {
                    "text": $key,
                    "freq": $facets($key),
                    "value": $key
                }
        }
    else
        []
};

declare function api:people-field($request as map(*)) {
    let $params := $request?parameters?person
    return
        <pb-combo-box close-after-select="">
            <select name="person" placeholder="Personen" multiple=""
                on-change="pb-search-resubmit">
            {
                let $refs := distinct-values(
                    for $hit in session:get-attribute($config:session-prefix || ".hits")
                    return
                        ft:field($hit, "person")
                )
                return (
                    for $ref in $refs
                    let $name := id($ref, doc($config:data-root || "/kbga-people.xml"))/tei:persName[@type="full"]/string()
                    order by $name[1]
                    return
                        <option value="{$ref}">
                        {
                            if ($ref = $params) then attribute selected { "selected" } else (),
                            $name
                        }
                        </option>,
                    for $param in $params
                    where not($param = $refs)
                    return
                        <option value="{$param}" selected="selected">
                        {
                            id($param, doc($config:data-root || "/kbga-people.xml"))/tei:persName[@type="full"]/string()
                        }
                        </option>
                    
                )
            }
            </select>
        </pb-combo-box>
};

declare function api:view-actor($request as map(*)) {
    let $name := 'kbga-actors-' || xmldb:decode($request?parameters?name)
    let $org := id($name, doc($config:data-root || "/kbga-organisations.xml"))
    let $entity :=
        if ($org) then
            $org
        else
            id($name, doc($config:data-root || "/kbga-people.xml"))
    let $source :=
        if ($org) then
            $config:data-root || "/kbga-organisations.xml"
        else
            $config:data-root || "/kbga-people.xml"
    return
        if ($entity) then
            let $entityName := 
                if ($org) then
                    $entity/tei:orgName[@type='full']
                else
                    $entity/tei:persName[@type="full"]
            let $template := doc($config:app-root || "/templates/pages/person.html")
            let $model := map { 
                "doc": $source,
                "xpath": '/id("' || $name || '")',
                "label": $entityName/string(),
                "key": $name,
                "category": substring($entityName, 1, 1),
                "view": if ($org) then 'organisations' else 'people',
                "template": "person.html"
            }
            return
                templates:apply($template, vapi:lookup#2, $model, tpu:get-template-config($request))
        else
            error($errors:NOT_FOUND, "Document " || $request?parameters?id || " not found")
};

declare function api:view-place($request as map(*)) {
    let $name := 'kbga-places-' || xmldb:decode($request?parameters?name)
    let $org := id($name, doc($config:data-root || "/kbga-places.xml"))
    let $entity :=
        if ($org) then
            $org
        else
            id($name, doc($config:data-root || "/kbga-places.xml"))
    let $source := $config:data-root || "/kbga-places.xml"
    return
        if ($entity) then
            let $entityName := $entity/tei:placeName[@type='full']
            let $coords := tokenize($entity/tei:location/tei:geo)
            let $template := doc($config:app-root || "/templates/pages/place.html")
            let $model := map { 
                "doc": $source,
                "xpath": '/id("' || $name || '")',
                "label": $entityName/string(),
                "key": $name,
                "category": substring($entityName, 1, 1),
                "latitude": $coords[1],
                "longitude": $coords[2],
                "view": "places",
                "template": "place.html"
            }
            return
                templates:apply($template, vapi:lookup#2, $model, tpu:get-template-config($request))
        else
            error($errors:NOT_FOUND, "Document " || $request?parameters?id || " not found")
};

declare function api:view-song($request as map(*)) {
    let $id := 'kbga-songs-' || xmldb:decode($request?parameters?id)
    let $source := $config:data-root || "/kbga-songs.xml"
    let $entity := id($id, doc($source))
    return
        if ($entity) then
            let $entityName :=  $entity/tei:title[1]/string()
            let $template := doc($config:app-root || "/templates/pages/song.html")
            let $model := map { 
                "doc": $source,
                "xpath": '/id("' || $id || '")',
                "label": $entityName,
                "key": $id,
                "category": substring($entityName, 1, 1),
                "view": 'songs',
                "template": "song.html"
            }
            return
                templates:apply($template, vapi:lookup#2, $model, tpu:get-template-config($request))
        else
            error($errors:NOT_FOUND, "Document " || $request?parameters?id || " not found")
};

declare function api:view-bibl($request as map(*)) {
    let $id := 'kbga-bibls-' || xmldb:decode($request?parameters?id)
    let $source := $config:data-root || "/kbga-bibls.xml"
    let $entity := doc($source)/id($id)
    return
        if ($entity) then
            let $entityName :=  $entity/tei:title[1]/string()
            let $template := doc($config:app-root || "/templates/pages/biblio.html")
            let $model := map { 
                "doc": $source,
                "xpath": '/id("' || $id || '")',
                "label": $entityName,
                "key": $id,
                "category": substring($entityName, 1, 1),
                "view": 'bibls',
                "template": "biblio.html"
            }
            return
                templates:apply($template, vapi:lookup#2, $model, tpu:get-template-config($request))
        else
            error($errors:NOT_FOUND, "Document " || $request?parameters?id || " not found")
};

declare function api:timeline($request as map(*)) {
    let $entries := session:get-attribute($config:session-prefix || '.hits')
    let $datedEntries := filter($entries, function($entry) {
            try {
                let $date := ft:field($entry, "date", "xs:date")
                return
                    exists($date) and year-from-date($date) != 1000
            } catch * {
                false()
            }
        })
    return
        map:merge(
            for $entry in $datedEntries
            group by $date := ft:field($entry, "date", "xs:date")
            return
                map:entry(format-date($date, "[Y0001]-[M01]-[D01]"), map {
                    "count": count($entry),
                    "info": ''
                })
        )
};

declare function api:bible-popover($request as map(*)) {
    let $target := xmldb:decode($request?parameters?target)
    let $range := tokenize($target, '\s+')
    let $citation := ext-html:bible-passage-citation($target)
    return
        <div>
            <p class="bible-source">{$citation}</p>
            <div>{bible:range($range[1], $range[2])}</div>
            <p class="bible-source">Übersetzung Martin Luther (Ausgabe 1912)</p>
        </div>
    };

declare function api:beacon($request as map(*)) {
    let $actors := doc($config:data-root || '/kbga-people.xml')//tei:person
    let $timeStamp := current-dateTime()
    return
('#FORMAT: BEACON
#PREFIX: https://d-nb.info/gnd/
#TARGET: https://kbga.karl-barth.ch/actors/{ID}
#FEED: https://kbga.karl-barth.ch/api/actors/beacon
#INSTITUTION: Karl Barth-Archiv
#CONTACT: Karl Barth-Archiv <bartharchiv-theol@unibas.ch>
#TimeStamp: '|| $timeStamp || '&#10;',
for $actor in $actors
let $gnd := $actor//tei:ptr[@type eq 'gnd']/@target => replace('^.+/gnd/([^/]+).*$', '$1')
let $ref := $actor/@xml:id/string()
let $id := $actor/substring-after($ref, 'kbga-actors-')
let $mentions := count(collection($config:data-root)//tei:persName[@ref=$ref])
where $gnd
return
    $gnd || '|' || $mentions || '|' || $id || '&#10;'
    )
};

xquery version "3.1";

module namespace bible="http://teipublisher.com/api/bible";

import module namespace pm-config="http://www.tei-c.org/tei-simple/pm-config" at "pm-config.xql";
import module namespace tei-nav="http://www.tei-c.org/tei-simple/navigation/tei" at "navigation-tei.xql";
import module namespace config="http://www.tei-c.org/tei-simple/config" at "config.xqm";
import module namespace ext-html="http://karlbarth.ch/apps/kb/ext-html.xql" at "ext-html.xql";

declare variable $bible:BOOKS := map {
    "Am": "AMO",
    "Apg": "ACT",
    "Bar": "BAR",
    "Bar 6": "BAR.6",
    "1.Chr": "1CH",
    "2.Chr": "2CH",
    "Dan": "DAN",
    "Eph": "EPH",
    "Esra": "EZR",
    "Est": "EST",
    "Gal": "GAL",
    "Geb.Man": "MAN",
    "Hab": "HAB",
    "Hag": "HAG",
    "Hebr": "HEB",
    "Hes": "EZK",
    "Hiob": "JOB",
    "Hld": "SNG",
    "Hos": "HOS",
    "Jak": "JAS",
    "Jdt": "JDT",
    "Jer": "JER",
    "Jes": "ISA",
    "Joel": "JOL",
    "Joh": "JHN",
    "1.Joh": "1JN",
    "2.Joh": "2JN",
    "3.Joh": "3JN",
    "Jona": "JON",
    "Jos": "JOS",
    "Jud": "JUD",
    "Klgl": "LAM",
    "Kol": "COL",
    "1.Kön": "1KI",
    "2.Kön": "2KI",
    "1.Kor": "1CO",
    "2.Kor": "2CO",
    "Lk": "LUK",
    "1.Makk": "1MA",
    "2.Makk": "2MA",
    "Mal": "MAL",
    "Mi": "MIC",
    "Mk": "MRK",
    "1.Mose": "GEN",
    "2.Mose": "EXO",
    "3.Mose": "LEV",
    "4.Mose": "NUM ",
    "5.Mose": "DEU",
    "Mt": "MAT",
    "Nah": "NAM",
    "Neh": "NEH",
    "Obd": "OBA",
    "Off": "REV",
    "1.Petr": "1PE",
    "2.Petr": "2PE",
    "Phil": "PHP",
    "Phlm": "PHM",
    "Pred": "ECC",
    "Ps": "PSA",
    "Ri": "JDG",
    "Röm": "ROM",
    "Rut": "RUT",
    "Sach": "ZEC",
    "1.Sam": "1SA ",
    "2.Sam": "2SA",
    "Sir": "SIR",
    "Spr": "PRO",
    "St zu Dan": "DAG",
    "St zu Est": "ESG",
    "1.Thess": "1TH",
    "2.Thess": "2TH",
    "1.Tim": "1TI",
    "2.Tim": "2TI ",
    "Tit": "TIT",
    "Tob": "TOB",
    "Weish": "WIS",
    "Zef": "ZEP Zef"
};

declare variable $bible:BIBLIA := json-doc($config:bible || '/luther-1912-sort.json');


declare function bible:fragment($request as map(*)) {
    let $id := xmldb:decode($request?parameters?id)
    let $range := tokenize($id, '[\s:]+')
    return
        bible:range($range[1], $range[2])
};

declare %private function bible:parse-ref($ref as xs:string) {
    let $groups := analyze-string($ref, "^((?:\d+\.)?[^\.]+)\.(\d+)(?:\.(\d+))?$")//fn:group
    let $book := $groups[1]/string()
    return
        $bible:BOOKS($book) || " " || string-join(subsequence($groups, 2), ":")
};

(: Alternative function to bible:parse-ref()  :)
declare %private function bible:parse-reference($ref as xs:string) {
    let $book := ext-html:get-book($ref)
    let $bookFile := $bible:BIBLIA?*[(?barth = $book) or (?abbreviations = $book)]?luther-1912
    let $chapter := ext-html:get-chapter($ref)
    let $verse := if (string-length(ext-html:get-verse($ref)) gt 0) then ext-html:get-verse($ref) else ()
    return
        substring-before($bookFile, '.xml') || " " || string-join(($chapter, $verse), ":")
};

declare function bible:range($start as xs:string, $end as xs:string?) {
    let $startId := bible:parse-reference($start)
    let $startElem := collection($config:bible)//*[@sid = $startId]
    let $endId := if ($end) then bible:parse-reference($end) else $startId
    let $endElem :=  collection($config:bible)//*[@eid = $endId]
    let $xml := if ($startElem) then bible:milestone-chunk($startElem, $endElem, root($startElem)/usx) else ()
    return
        if ($xml) then 
        $pm-config:web-transform($xml, map { "root": root($startElem) }, "usx.odd") else <span>Verse reference not found</span>
};

declare %private function bible:milestone-chunk($ms1 as element(), $ms2 as element()?, $node as node()*) as node()* {
    let $descendantCheck :=
        if ($ms1 instance of element(chapter) and (empty($ms2) or $ms2 instance of element(chapter))) then
            function($node, $ms1, $ms2) {
                $node/descendant::chapter intersect ($ms1, $ms2)
            }
        else
            function($node, $ms1, $ms2) {
                some $n in $node/descendant::* satisfies ($n is $ms1 or $n is $ms2)
            }
    return
        tei-nav:milestone-chunk($ms1, $ms2, $node, $descendantCheck)
};


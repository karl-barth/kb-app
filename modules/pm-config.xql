
xquery version "3.1";

module namespace pm-config="http://www.tei-c.org/tei-simple/pm-config";

import module namespace pm-barth-web="http://www.tei-c.org/pm/models/barth/web/module" at "../transform/barth-web-module.xql";
import module namespace pm-barth-predigten-web="http://www.tei-c.org/pm/models/barth-predigten/web/module" at "../transform/barth-predigten-web-module.xql";
import module namespace pm-barth-buch-web="http://www.tei-c.org/pm/models/barth-buch/web/module" at "../transform/barth-buch-web-module.xql";
import module namespace pm-barth-register-web="http://www.tei-c.org/pm/models/barth-register/web/module" at "../transform/barth-register-web-module.xql";
import module namespace pm-barth-vortrag-web="http://www.tei-c.org/pm/models/barth-vortrag/web/module" at "../transform/barth-vortrag-web-module.xql";
import module namespace pm-docx-tei="http://www.tei-c.org/pm/models/docx/tei/module" at "../transform/docx-tei-module.xql";
import module namespace pm-barth-briefe-web="http://www.tei-c.org/pm/models/barth-briefe/web/module" at "../transform/barth-briefe-web-module.xql";
import module namespace pm-usx-web="http://www.tei-c.org/pm/models/usx/web/module" at "../transform/usx-web-module.xql";
import module namespace pm-usx-print="http://www.tei-c.org/pm/models/usx/print/module" at "../transform/usx-print-module.xql";
import module namespace pm-usx-latex="http://www.tei-c.org/pm/models/usx/latex/module" at "../transform/usx-latex-module.xql";
import module namespace pm-usx-epub="http://www.tei-c.org/pm/models/usx/epub/module" at "../transform/usx-epub-module.xql";
import module namespace pm-usx-fo="http://www.tei-c.org/pm/models/usx/fo/module" at "../transform/usx-fo-module.xql";

declare variable $pm-config:web-transform := function($xml as node()*, $parameters as map(*)?, $odd as xs:string?) {
    switch ($odd)
    case "barth.odd" return pm-barth-web:transform($xml, $parameters)
case "barth-predigten.odd" return pm-barth-predigten-web:transform($xml, $parameters)
case "barth-buch.odd" return pm-barth-buch-web:transform($xml, $parameters)
case "barth-register.odd" return pm-barth-register-web:transform($xml, $parameters)
case "barth-vortrag.odd" return pm-barth-vortrag-web:transform($xml, $parameters)
case "barth-briefe.odd" return pm-barth-briefe-web:transform($xml, $parameters)
case "usx.odd" return pm-usx-web:transform($xml, $parameters)
    default return pm-barth-web:transform($xml, $parameters)
            
    
};
            


declare variable $pm-config:print-transform := function($xml as node()*, $parameters as map(*)?, $odd as xs:string?) {
    switch ($odd)
    case "usx.odd" return pm-usx-print:transform($xml, $parameters)
    default return error(QName("http://www.tei-c.org/tei-simple/pm-config", "error"), "No default ODD found for output mode print")
            
    
};
            


declare variable $pm-config:latex-transform := function($xml as node()*, $parameters as map(*)?, $odd as xs:string?) {
    switch ($odd)
    case "usx.odd" return pm-usx-latex:transform($xml, $parameters)
    default return error(QName("http://www.tei-c.org/tei-simple/pm-config", "error"), "No default ODD found for output mode latex")
            
    
};
            


declare variable $pm-config:epub-transform := function($xml as node()*, $parameters as map(*)?, $odd as xs:string?) {
    switch ($odd)
    case "usx.odd" return pm-usx-epub:transform($xml, $parameters)
    default return error(QName("http://www.tei-c.org/tei-simple/pm-config", "error"), "No default ODD found for output mode epub")
            
    
};
            


declare variable $pm-config:fo-transform := function($xml as node()*, $parameters as map(*)?, $odd as xs:string?) {
    switch ($odd)
    case "usx.odd" return pm-usx-fo:transform($xml, $parameters)
    default return error(QName("http://www.tei-c.org/tei-simple/pm-config", "error"), "No default ODD found for output mode fo")
            
    
};
            


declare variable $pm-config:tei-transform := function($xml as node()*, $parameters as map(*)?, $odd as xs:string?) {
    switch ($odd)
    case "docx.odd" return pm-docx-tei:transform($xml, $parameters)
    default return error(QName("http://www.tei-c.org/tei-simple/pm-config", "error"), "No default ODD found for output mode tei")
            
    
};
            
    
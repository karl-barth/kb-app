xquery version "3.1";

(: Module for app-specific template functions :)
module namespace app="teipublisher.com/app";

import module namespace bible="http://teipublisher.com/api/bible" at "bible.xql";
import module namespace templates="http://exist-db.org/xquery/html-templating";
import module namespace pages="http://www.tei-c.org/tei-simple/pages" at "lib/pages.xql";
import module namespace kwic="http://exist-db.org/xquery/kwic" at "resource:org/exist/xquery/lib/kwic.xql";
import module namespace nav="http://www.tei-c.org/tei-simple/navigation" at "navigation.xql";
import module namespace query="http://www.tei-c.org/tei-simple/query" at "query.xql";
import module namespace browse="http://www.tei-c.org/tei-simple/templates" at "lib/browse.xql";
import module namespace config="http://www.tei-c.org/tei-simple/config" at "config.xqm";
import module namespace ext-html="http://karlbarth.ch/apps/kb/ext-html.xql" at "ext-html.xql";
import module namespace pm-config="http://www.tei-c.org/tei-simple/pm-config" at "pm-config.xql";
import module namespace tpu="http://www.tei-c.org/tei-publisher/util" at "lib/util.xql";

declare namespace tei="http://www.tei-c.org/ns/1.0";

declare function app:data-version($node as node(), $model as map(*)) {
    let $repo := doc(concat($config:data-root, "/repo.xml"))/repo:meta
    return
        if ($repo/repo:target = "kb-latest-version") then
            <p>Daten vom {format-date(xs:dateTime($repo/repo:deployed), "[D]. [MNn] [Y]")}</p>
        else
            ()
};

declare
    %templates:wrap
function app:volumes($node as node(), $model as map(*)) {
    for $hits in $model?all
    group by $volume := ft:field($hits, "volume")
    where $volume != ''
    let $header := root($hits[1])//tei:titleStmt/tei:title[@type="volume"]
    let $volnum := number(replace($volume, "^.*?(\d+)$", "$1"))
    return
        <li class="document">
            <h4>Band {$volnum}</h4>
            <h3><a href="#" data-collection="{$volume}">{$header/text()}</a></h3>
        </li>,
    <li class="document">
        <h4>Konvertierung</h4>
        <h3><a href="#" data-collection="upload">Word Files</a></h3>
    </li>
};

declare function app:toc-toggle($node as node(), $model as map(*)) {
    if (matches($model?doc, "^vol-4[07]/.*")) then
        $node
    else
        ()
};

declare function app:show-hits($node as node(), $model as map(*), $query as xs:string?) {
    if ($query and $query != '') then
        let $docId := config:get-identifier($model?work)
        let $config := tpu:parse-pi(root($model?work), "div")
        let $expanded := query:expand($config, $model?work)
        let $matches := $expanded//exist:match
        where count($matches) > 0
        return
            <div class="matches">
                <div class="count">[{count($matches)} Treffer]</div>
            {
                for $match in subsequence($matches, 1, 5)
                let $matchId := $match/../@exist:id
                let $div := nav:get-section-for-node($config, util:node-by-id($model?work, $matchId))
                let $config := <config width="60" table="no" link="{$docId}?root={util:node-id($div)}&amp;action=search&amp;view={$config?view}&amp;odd={$config?odd}#{$matchId}"/>
                return
                    kwic:get-summary($expanded, $match, $config)
            }
            </div>
    else
        ()
};

(:~
 : List documents in data collection
 :)
declare
    %templates:wrap
    %templates:default("sort", "last-modified")
function app:list-works($node as node(), $model as map(*), $filter as xs:string?, $browse as xs:string?, 
    $odd as xs:string?, $sort as xs:string, $query as xs:string?, $page as xs:int?) {
    let $params := browse:params2map($model?root)
    let $cached := session:get-attribute($config:session-prefix || ".works")
    let $filtered :=
        if (exists($page) and exists($cached)) then
            $cached
        else if (exists($filter) and $filter != '') then
            query:query-metadata($browse, $filter, $sort)
        else if (exists($query) and $query != '') then
            query:query-default("text", $query, (), $sort)
        else
            query:query-metadata($browse, $filter, $sort)
    let $sorted := browse:sort($filtered, $sort)
    return (
        session:set-attribute($config:session-prefix || ".timestamp", current-dateTime()),
        session:set-attribute($config:session-prefix || '.hits', $filtered),
        session:set-attribute($config:session-prefix || ".query", $query),
        session:set-attribute($config:session-prefix || ".field", "text"),
        if (exists($page) and exists($cached)) then
            ()
        else
            session:set-attribute($config:session-prefix || '.params', $params),
        session:set-attribute($config:session-prefix || ".works", $sorted),
        map {
            "all" : $sorted,
            "mode": "browse"
        }
    )
};

declare 
    %templates:wrap
function app:entity-mentions($node as node(), $model as map(*)) {
    let $mentions :=
        switch ($model?view)
            case 'organisations' return
                collection($config:data-root)//tei:orgName[@ref=$model?key]
            case 'places' return
                collection($config:data-root)//tei:placeName[@ref=$model?key]
            case 'songs' return 
                collection($config:data-root)//tei:bibl[@corresp=$model?key]
            case 'bibls' return 
                collection($config:data-root)//tei:bibl[@corresp=$model?key]
            case 'people' return
                collection($config:data-root)//tei:persName[@ref=$model?key]
            default return
                collection($config:data-root)//tei:ref[@subtype eq 'bible'][@target=$model?target]
    return
        if (count($mentions) > 0) then (
            <h3>Vorkommen in Texten</h3>,
            <ul>
            {
                for $mention in $mentions
                let $count := if ($mention/@ref) then count(root($mention)/descendant::*[@ref eq $mention/@ref]) else if ($mention/@corresp) then count(root($mention)/descendant::*[@corresp eq $mention/@corresp]) else
                    count(root($mention)/descendant::*[@target eq $mention/@target])
                group by $id := root($mention)/tei:TEI/@xml:id
                let $link := $model?app || "/texts/" || substring-after($id, 'kbga-texts-') 
                return
                    <li>
                        <a href="{$link}">
                        { root(head($mention))//tei:titleStmt/tei:title[@type="citation_line_1"]/string() }
                        </a> <span>{' [' || $count[1] || ' Treffer]'}</span>
                    </li>
            }
            </ul>
        ) else
            ()
};

declare 
    %templates:wrap
function app:entity-publications($node as node(), $model as map(*)) {
    let $publications := doc($config:data-root || '/kbga-bibls.xml')//tei:bibl[ft:query(., 'status:autorisiert')][descendant::*[@ref = $model?key]]
    return
        if (count($publications) gt 0) then (
            <h3>Veröffentlichungen</h3>, 
            <ul>
                {for $bibl in $publications
                 let $title := ext-html:output-reference($bibl, 80, 'inactive')
                 order by $bibl
                return
                    <li><a href="../biblio/{substring-after($bibl/@xml:id, 'kbga-bibls-')}">{$title}</a></li>}
            </ul>) else ()
    
};
(: Bible register :)

declare  
%templates:wrap
%templates:default("id","")
function app:load-book($node as node(), $model as map(*), $id as xs:string) {
    let $barth := xmldb:decode($id)
    return 
        $bible:BIBLIA?*[?barth = $barth]
};

declare  
%templates:wrap
%templates:default("book","")
%templates:default("id","")
function app:load-passage($node as node(), $model as map(*), $book as xs:string,  $id as xs:string) {
    let $barth := xmldb:decode($book)
    let $target := xmldb:decode($id)
    let $locus := ext-html:get-locus($target)
    let $mapBook := $bible:BIBLIA?*[?barth = $barth]
    return 
        map:merge(($mapBook,  map {"target" : $target, "citation" : $barth|| '. ' || $locus}))
};

declare  
%templates:wrap
function app:bible-label($node as node(), $model as map(*)) {
        $model?name
};

declare  
%templates:wrap
function app:bible-book-link($node as node(), $model as map(*)) {
        <a href="../{$model?barth}">{$model?name}</a>
};

declare  
%templates:wrap
function app:bible-passage($node as node(), $model as map(*)) {
       $model?citation
};

declare  
%templates:wrap
function app:view-bible-book($node as node(), $model as map(*)) {
    let $abbreviations := for $x in $model?abbreviations return $x
    let $labels := ($abbreviations, $model?barth)
    let $title := $model?name
    let $abbreviation := $model?barth
    let $bibls := collection($config:data-root)//tei:ref[@subtype eq 'bible']
    let $query := 'book:' || string-join($labels, ' OR book:')
    let $passages := $bibls[ft:query-field(., $query)]
    let $sortedByVerse := sort($passages, "?lang=de-DE", function ($ref) {format-number(number(ft:field($ref, 'verse')[1]), '000')})
    let $sorted := sort($sortedByVerse, "?lang=de-DE", function ($ref) {format-number(number(ft:field($ref, 'chapter')[1]), '000')})
    return
        <div class="register">
            <h1>{$title}</h1>
            <div class="register-list">{
                if ($passages) then 
                     for $passage in distinct-values($sorted/ft:field(., 'locus')) 
                     let $target := if ($passage) then $bibls[ft:query-field(., 'locus:' || $passage || ' AND book:' || $abbreviation)]/@target else ()
                    return 
                        if ($target) then
                        <a href="{$abbreviation}/{$target[1]}" class="split-list-item">{$abbreviation || '. ' || $passage}</a> else ()
                else 'Dieses Buch wird im Korpus nicht zitiert.'
                    }</div>
            </div>
};

(:  declare  
%templates:wrap
function app:view-bible-passage($node as node(), $model as map(*)) {
   let $targets := tokenize($model?target)
   return
    <div>
        <h1>{$model?citation}</h1>
        <div>{
        if (count($targets) > 1) then
            let $first := id('Lb_' || $targets[1], doc($config:data-root || "/luther-bibel.xml"))
            let $last := id('Lb_' || $targets[2], doc($config:data-root || "/luther-bibel.xml"))
            let $content := <tei:div type="book">{($first, $first/following::tei:ab[. << $last], $last)}</tei:div>
            return
                <div>{$pm-config:web-transform(
                            $content,
                            map { 
                                "root": $content, 
                                "view": "single", 
                                "webcomponents": 7},
                                'barth.odd')}</div>
                else 
                    let $passage := id('Lb_' || $targets, doc($config:data-root || "/luther-bibel.xml"))
                    let $content := <tei:div type="book">{$passage}</tei:div>
                    return
                        <div>{$pm-config:web-transform(
                            $content,
                            map { 
                                "root": $content, 
                                "view": "single", 
                                "webcomponents": 7},
                                'barth.odd')}</div>
                    }
        </div>
    </div>
};
:)

declare  
%templates:wrap
function app:view-bible-passage($node as node(), $model as map(*)) {
    let $target := $model?target
    let $range := tokenize($target, '[\s:]+')
    return
    <div>
        {bible:range($range[1], $range[2])}
    </div>
};

(:~
 : Hack to adjust text width in synoptic view
 :)
declare function app:css($node as node(), $model as map(*)) {
    let $data := config:get-document($model?doc)
    return
        if ($data//tei:div[@rend="right"]) then
            <style>
            .content-view pb-view {{
                max-width: var(--synoptic-max-width);
            }}
            </style>
        else
            ()
};
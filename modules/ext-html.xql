xquery version "3.1";

module namespace pmf="http://karlbarth.ch/apps/kb/ext-html.xql";

declare namespace tei="http://www.tei-c.org/ns/1.0";

import module namespace html="http://www.tei-c.org/tei-simple/xquery/functions";
import module namespace config="http://www.tei-c.org/tei-simple/config" at "config.xqm";


declare function pmf:get-entity($id as xs:string, $type as xs:string){
    id($id, doc($config:data-root || "/kbga-" || $type || ".xml"))
    };

declare function pmf:bible-quote($config as map(*), $node as element(), $class as xs:string+, $content, $target) {
       <pb-popover persistent="persistent" remote="api/bible-popover/{$target}" class="bible-passage">{$content/string()}<span slot="alternate">Loading ...</span></pb-popover>
}; 

declare function pmf:bible-register($config as map(*), $node as element(), $class as xs:string+, $content, $target) {
    let $citation := pmf:bible-passage-citation($target)
    return
        <pb-popover persistent="persistent" remote="api/bible-popover/{$target}" class="bible-passage register">{$citation}<span slot="alternate">Loading ...</span></pb-popover>
                
}; 

declare function pmf:bible-passage-citation($target) {
    pmf:get-book($target) || '. ' || pmf:get-locus($target)
    };

declare function pmf:output-terms-div($terms as element()*){
    let $items := pmf:output-terms($terms)
    return if ($items) then
       <div class="metadata">
          <h3>Begriffe</h3>
          {$items}
    </div>
else ()
};

declare function pmf:output-terms($terms as element()*) {
    let $refs :=
            for $term in $terms
            group by $ref := $term/@ref
            return
                $ref
    for $ref at $p in $refs
    let $cat := id($ref, doc($config:data-root || '/kbga-terms.xml'))
    let $title := string-join($cat/ancestor-or-self::tei:category/tei:catDesc[1], " &#187; ")
    return  if ($cat) then (
        if ($p > 1) then
            text { " &#x2022; " }
        else
            (),
        <span class="category">{$title}</span>
    )
    else ()
};

declare function pmf:output-entity($node as element()) {
    let $ref := $node/@ref
    return
        if ($ref) then
            if ($node/ancestor::tei:note) then
                let $first := head(($node/ancestor::tei:note//*[@ref=$ref]))
                return
                    $node >> $first
            else
                let $pb := $node/preceding::tei:pb[@ed='pga'][1]
                return
                    $node/preceding::*[@ref = $ref][not(ancestor::tei:note)][ancestor::tei:div[1] is $node/ancestor::tei:div[1]][. >> $pb]
        else
            ()
};

declare function pmf:output-bibl($node as element()) {
    let $ref := $node/@corresp
    return
        if ($ref) then
            if ($node/ancestor::tei:note) then
                let $first := head(($node/ancestor::tei:note//*[@corresp=$ref]))
                return
                    $node >> $first
            else
                let $pb := $node/preceding::tei:pb[@ed='pga'][1]
                return 
                    $node/preceding::*[@corresp = $ref][not(ancestor::tei:note)][. >> $pb]
        else
            ()
};

declare function pmf:output-authors($node as element()) {
    let $authors:= if ($node/tei:author) then $node/tei:author else $node/tei:editor
    let $links := if ($authors) then for $author in $authors return pmf:output-author-link($author) else ()
    let $label:= if (count($authors) >= 1) then 'von ' else ''
    let $first := head($links)
    let $last := if (count($links) gt 1) then $links[last()] else ()
    let $mid := tail($links[not(position() eq last())])
    let $formattedLast := if ($last) then (' und ', $last) else ()
    let $formattedMid := if ($mid) then for $x in $mid return ('; ', $x) else ()
    return 
        ($label, $first, $formattedMid, $formattedLast)
    };
    
declare function pmf:output-author-link($node as element()) {
    let $id := $node/@ref
    let $person := id($id, doc($config:data-root || '/kbga-people.xml'))
    let $name := $person/tei:persName[1]
    return 
        <a href="../actors/{substring-after($id, 'actors-')}">{$name}</a>
    };

declare function pmf:get-authors($node as element(), $type as xs:string) {
    switch ($type)
        case 'active' return subsequence(pmf:output-authors($node), 2)
        default return 
            if ($node/tei:author) then string-join($node/tei:author, '; ') else if 
                ($node/tei:editor) then string-join($node/tei:editor, '; ') || ' (Hrsg.)' else ()
};

declare function pmf:output-reference($node as element(), $limit as xs:integer, $type as xs:string) {
    let $bibl := if ($node/@xml:id) then $node else id($node/@corresp, doc($config:data-root || '/kbga-bibls.xml'))
    let $author := pmf:get-authors($node, $type)
    let $formattedAuthor := if (ends-with($author, '.')) then ($author,  ' ') else if ($type eq 'inactive') then $author || '. ' else ($author, '. ') 
    let $fullTitle := $bibl/tei:title[1]
    let $title := if (string-length($fullTitle) gt $limit) then substring($fullTitle, 1, $limit) || '[…]' else $fullTitle/string()
    let $formattedTitle := if (not(ends-with($title, '.'))) then $title || '. ' else $title || ' '
    let $date := if ($bibl/tei:date) then $bibl/replace(tei:date[1], '.*(\d{4}$)', '$1') else if ($bibl/tei:edition) then $bibl/tei:edition[1]/string() else ()
    return
        (if ($author) then $formattedAuthor else (), if ($bibl/@type eq 'song') then $formattedTitle else <em>{$formattedTitle}</em>, $date)
    };

declare function pmf:output-song-book($node as element()) {
    let $content := $node/string()
    return
      if ($node/@corresp) then
        let $ref := $node/@corresp
        return <li>
            <pb-popover remote="api/bibls/{$ref}"><span>{$content} <iron-icon slot="default" icon="av:library-books" class="iron-icon-small"/></span>s</pb-popover></li>
        else <li>{$content}</li>
    };
(:<a href="biblio/{substring-after($ref, 'bibls-')}">{$title}</a>:)

declare function pmf:format-date($dateStr as xs:string?) {
    if (not($dateStr) or $dateStr = '') then
        ()
    else if (matches($dateStr, "^\d{4}-\d{2}-\d{2}")) then
        format-date($dateStr, '[D01]. [MNn] [Y0001]', 'de', (), ())
    else
        substring($dateStr, 1, 4)
};

declare function pmf:document-type($key as xs:string) {
    switch ($key)
        case "letter" return "Brief"
        case "chapter" return "Buchkapitel"
        case "review" return "Rezension"
        case "sermon" return "Predigt"
        case "paper" return "Vortrag oder kleinere Arbeit"
        case "lecture" return "Vorlesung"
        case "preface" return "Vorwort der Herausgeber"
        default return "Andere"
};

declare function pmf:output-link($current as element(), $direction as xs:string) {
    let $all := session:get-attribute($config:session-prefix || ".works")
    let $pos := pmf:position($all, root($current)/tei:TEI/@xml:id, 1)
    where if ($direction = 'next') then $pos < count($all) else $pos > 1
    let $item :=
        if ($direction = 'previous') then
            $all[$pos - 1]
        else
            $all[$pos + 1]
    let $docConfig := config:default-config(document-uri(root($item)))
    let $title := root($item)//tei:teiHeader//tei:title[@type='citation_line_1']/string()
    return
        <pb-link path="texts/{substring-after(root($item)/tei:TEI/@xml:id, 'kbga-texts-')}" emit="transcription" 
            odd="{substring-before($docConfig?odd, '.odd')}" view="{$docConfig?view}" class="part-nav">
            <pb-popover>
                <paper-icon-button icon="{if ($direction = 'next') then 'icons:last-page' else 'icons:first-page'}" 
                    alt="{$title}"/>
                <span slot="alternate">{$title}</span>
            </pb-popover>
        </pb-link>
};

declare function pmf:position($list as element()*, $current as xs:string, $pos as xs:int) {
    if (empty($list)) then
        ()
    else
        let $next := head($list)
        return
            if (root($next)/tei:TEI/@xml:id = $current) then
                $pos
            else
                pmf:position(tail($list), $current, $pos + 1)
};

declare function pmf:latDezi($n as xs:integer) as xs:string
{
    string-join(
        if ($n >= 500) then
            ("D", pmf:latDezi($n - 500))
        else if ($n >= 400) then
            ("CD", pmf:latDezi($n - 400))    
        else if ($n >= 100) then
            ("C", pmf:latDezi($n - 100))
        else if ($n >= 50) then
            ("L", pmf:latDezi($n - 50))
        else if ($n >= 40) then
            ("XL", pmf:latDezi($n - 40))
        else if ($n >= 10) then
            ("X", pmf:latDezi($n - 10))
        else if ($n >= 9) then
            ("IX", pmf:latDezi($n - 9))
        else if ($n >= 5) then
            ("V", pmf:latDezi($n - 5))
        else if ($n >= 4) then
            ("IV", pmf:latDezi($n - 4))
        else
            for $i in 1 to $n return "I"
    )
};


declare function pmf:get-book($target as xs:string) {
    let $tokens := tokenize($target, '\.')
    return
    if (matches($tokens[1], '^[1-3]')) then string-join(subsequence($tokens, 1, 2), '.')  else $tokens[1]
};

declare function pmf:get-chapter($target as xs:string) {
    for $ref in tokenize($target, '\s+') 
    let $tokens := tokenize($ref, '\.')
    return 
    if (matches($tokens[1], '^[1-3]')) then $tokens[3] else $tokens[2]
};

declare function pmf:get-verse($target as xs:string) {
    for $ref in tokenize($target, '\s+')
    let $tokens := tokenize($ref, '\.')
    return 
        if (matches($tokens[1], '^[1-3]')) then $tokens[4] else $tokens[3]

};

declare function pmf:get-locus($target as xs:string) {
    let $chapters := distinct-values(pmf:get-chapter($target))
    let $verses := pmf:get-verse($target)
    return
        if ((count($chapters) gt 1) and (string-length($verses[1]) gt 0)) then
            $chapters[1] || ',' || $verses[1] || '–' || $chapters[2] || ',' || $verses[2] 
        else 
            if (count($chapters) gt 1) then string-join($chapters, '–')
            else 
                if (string-length($verses[1]) gt 0) then $chapters[1] || ',' || string-join($verses, '–') 
                else string-join($chapters)
   };
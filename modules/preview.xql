xquery version "3.1";

import module namespace docx="http://existsolutions.com/teipublisher/docx";
import module namespace pm-config="http://www.tei-c.org/tei-simple/pm-config" at "pm-config.xql";
import module namespace tpu="http://www.tei-c.org/tei-publisher/util" at "util.xql";
import module namespace nav="http://www.tei-c.org/tei-simple/navigation" at "navigation.xql";
import module namespace config="http://www.tei-c.org/tei-simple/config" at "config.xqm";

declare namespace tei="http://www.tei-c.org/ns/1.0";
declare namespace output="http://www.w3.org/2010/xslt-xquery-serialization";


declare %private function local:postprocess($nodes as node()*, $styles as element()?, $odd as xs:string) {
    let $oddName := replace($odd, "^.*/([^/\.]+)\.?.*$", "$1")
    for $node in $nodes
    return
        typeswitch($node)
            case element(head) return
                element { node-name($node) } {
                    $node/@*,
                    $node/node(),
                    <link rel="stylesheet" type="text/css" href="../transform/{replace($oddName, "^(.*)\.odd$", "$1")}-print.css" media="print"/>,
                    $styles
                }
            case element(body) return
                element { node-name($node) } {
                    $node/@*,
                    local:postprocess($node/node(), $styles, $odd),
                    nav:output-footnotes(root($node)//*[@class = "footnote"])
                }
            case element() return
                if ($node/@class = "footnote") then
                    ()
                else
                    element { node-name($node) } {
                        $node/@*,
                        local:postprocess($node/node(), $styles, $odd)
                    }
            default return
                $node
};

declare function local:preview($body, $odd as xs:string) {
    let $config := tpu:parse-pi($body, (), $odd)
    let $html := $pm-config:web-transform($body, map { "root": $body, "webcomponents": 7 }, $config?odd)
    return
        local:postprocess($html, (), $config?odd)
};

declare function local:convert($body, $odd as xs:string) {
    let $transform := $pm-config:tei-transform(?, ?, $odd)
    return
        docx:process-pkg($body, $transform)
};

let $action := request:get-parameter("action", "convert")
let $indent := request:get-parameter("indent", "false")
let $body := request:get-data()
let $odd := request:get-parameter("odd", "docx-preview.odd")
let $result :=
    switch ($action)
        case "preview" return local:preview($body, $odd)
        default return local:convert($body, "docx.odd")
return (
    util:declare-option("output:indent", if ($indent = "true") then "yes" else "no"),
    $result
)